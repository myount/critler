package me.yount.michael.critler;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CritAdapter extends ArrayAdapter<Crit> {
    Context context;
    int layoutResourceId;
    Crit[] data = null;

    public CritAdapter(Context context, int layoutResourceId, Crit[] data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        CritHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new CritHolder();
            holder.imgIcon = (ImageView)(row.findViewById(R.id.crit_icon));
            holder.tvTitle = (TextView)(row.findViewById(R.id.crit_title));
            holder.tvDescription = (TextView)(row.findViewById(R.id.crit_desc));

            row.setTag(holder);
        } else {
            holder = (CritHolder)(row.getTag());
        }

        Crit crit = data[position];
        holder.imgIcon.setImageResource(crit.icon);
        holder.imgIcon.setColorFilter(crit.getType().getColor());
        holder.tvTitle.setText(crit.title);
        holder.tvDescription.setText(Html.fromHtml(crit.description));

        return row;
    }

    private class CritHolder {
        ImageView imgIcon;
        TextView tvTitle, tvDescription;
    }
}
