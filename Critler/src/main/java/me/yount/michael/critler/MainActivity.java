package me.yount.michael.critler;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

public class MainActivity extends Activity {

    private TtsInitListener mTtsInitListener = new TtsInitListener();
    private ArrayList<Crit> mCritsDrawn = new ArrayList<Crit>();
    private TextToSpeech mTts;
    private final int TTS_CHECK_CODE = 0;
    private boolean mTtsEnabled = false, mTtsAutoRead = false;
    private SharedPreferences prefs;
    private SharedPreferences.Editor prefsEditor;
    private final ButtonClickListener mButtonClickListener = new ButtonClickListener();
    private CardDatabaseHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbHelper = new CardDatabaseHelper(this);

        prefs = getPreferences(MODE_PRIVATE);
        prefsEditor = prefs.edit();

        mButtonClickListener.setContext(this);
        findViewById(R.id.button_crit_bludgeon).setOnClickListener(mButtonClickListener);
        findViewById(R.id.button_crit_pierce).setOnClickListener(mButtonClickListener);
        findViewById(R.id.button_crit_slash).setOnClickListener(mButtonClickListener);
        findViewById(R.id.button_crit_magic).setOnClickListener(mButtonClickListener);
        findViewById(R.id.button_fumble_melee).setOnClickListener(mButtonClickListener);
        findViewById(R.id.button_fumble_natural).setOnClickListener(mButtonClickListener);
        findViewById(R.id.button_fumble_ranged).setOnClickListener(mButtonClickListener);
        findViewById(R.id.button_fumble_magic).setOnClickListener(mButtonClickListener);

        ListView critsDrawn = (ListView)findViewById(R.id.crits_drawn);
        critsDrawn.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (mTtsInitListener.isTtsInitialized() && mTtsEnabled) {
                    Crit item = (Crit)adapterView.getItemAtPosition(i);
                    if (item != null) {
                        mTts.speak(sanitizeForTts(item.title + ". " + item.description),
                                TextToSpeech.QUEUE_FLUSH, null);
                    }
                }
            }
        });

        Intent checkIntent = new Intent();
        checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(checkIntent, TTS_CHECK_CODE);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == TTS_CHECK_CODE) {
            if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
                mTts = new TextToSpeech(this, mTtsInitListener);
            } else {
                String availableVoices = data.getStringArrayListExtra(TextToSpeech.Engine.EXTRA_AVAILABLE_VOICES).toString();
                if (!availableVoices.contains("eng")) {
                    AlertDialog noTts = (new AlertDialog.Builder(MainActivity.this))
                            .setTitle(getString(R.string.dialog_no_tts_title))
                            .setMessage(getString(R.string.dialog_no_tts_body))
                            .setCancelable(false)
                            .setPositiveButton(getString(R.string.dialog_no_tts_button_install), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                    Toast.makeText(MainActivity.this, getString(R.string.dialog_no_tts_toast_launching_tts_install), Toast.LENGTH_LONG).show();

                                    Intent installIntent = new Intent();
                                    installIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                                    startActivity(installIntent);
                                }
                            })
                            .setNeutralButton(getString(R.string.dialog_no_tts_button_later), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    AlertDialog later = (new AlertDialog.Builder(MainActivity.this))
                                            .setTitle(getString(R.string.dialog_no_tts_skip_title))
                                            .setMessage(getString(R.string.dialog_no_tts_skip_prompt_warning))
                                            .setPositiveButton(getString(R.string.dialog_no_tts_skip_button_ok), new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    dialogInterface.dismiss();
                                                }
                                            })
                                            .create();
                                    later.show();
                                }
                            })
                            .setNegativeButton(getString(R.string.dialog_no_tts_button_never), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    prefsEditor.putBoolean("tts", false);
                                    dialogInterface.dismiss();
                                }
                            })
                            .create();
                    noTts.show();
                } else {
                    mTts = new TextToSpeech(this, mTtsInitListener);
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public boolean onPrepareOptionsMenu(final Menu menu) {
        MenuItem menuItemEnableTts = menu.findItem(R.id.option_tts);
        boolean isTtsEnabled = prefs.getBoolean("tts", true);
        menuItemEnableTts.setChecked(isTtsEnabled);
        menu.findItem(R.id.option_auto_read).setEnabled(isTtsEnabled);
        if (isTtsEnabled) menu.findItem(R.id.option_auto_read).setChecked(prefs.getBoolean("auto_read",false));
        else menu.findItem(R.id.option_auto_read).setChecked(false).setEnabled(false);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_clear:
                mCritsDrawn.clear();
                if (mTts.isSpeaking()) mTts.stop();
                ((ListView)findViewById(R.id.crits_drawn)).setAdapter(new CritAdapter(this, R.layout.drawn_crit, new Crit[] {}));
                return true;

            case R.id.action_about:
                Intent intent = new Intent(this,AboutActivity.class);
                startActivity(intent);
                return true;

            case R.id.option_tts:
                boolean ttsInitialCheckedState = item.isChecked();
                item.setChecked(!ttsInitialCheckedState);
                prefsEditor.putBoolean("tts", !ttsInitialCheckedState);
                prefsEditor.commit();
                mTtsEnabled = !ttsInitialCheckedState;
                return true;

            case R.id.option_auto_read:
                boolean autoReadInitialCheckedState = item.isChecked();
                mTtsAutoRead = !autoReadInitialCheckedState;
                item.setChecked(!autoReadInitialCheckedState);
                prefsEditor.putBoolean("auto_read", !autoReadInitialCheckedState);
                prefsEditor.commit();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onDestroy() {
        mTts.shutdown();
        dbHelper.close();
        super.onDestroy();
    }

    private String sanitizeForTts(String input) {
        // TODO: Figure out the correct SSML incantation to make the default TTS engine pronounce "target[']s" correctly.
        // Because the one commented out below isn't it, and it grates on me every time it says
        // "tarjit[']s".

        return input.replaceAll(" Str ",   " Strength ") // otherwise it spells out "S-T-R"
                    .replaceAll(" Cha ",   " Charisma ") // "Cha" sounds silly
                    .replaceAll("\\(Ref ", "(Reflex ")
                    .replaceAll("<(/)?em>", "")
//                    .replaceAll("target'?s"," <phoneme alphabet=\"xsampa\" ph=\"tAr\\%gIts\" /> ")
                    .replaceAll(" nonlethal "," non-lethal ") // otherwise it pronounces the e like "eh"
                    .replaceAll(" AC "," A.C. ") // otherwise it says "ack"
                    .replaceAll(" DR (\\d)/- ", " D.R. $1 dash ") // otherwise it says "doctor" for "DR"
                    .replaceAll(" DR (\\d)/(.+) "," D.R. $1 $2")  // Id.
                    .replaceAll(" DR[\\.\\)]?", " D.R. ") // just in case the last two didn't catch it
                    .replaceAll(" SR ", " S.R. ")
                    .replaceAll(" SR[\\.\\)]?", " S.R. ") // just in case the last one didn't catch it
                ;
    }

    public class ButtonClickListener implements View.OnClickListener {
        private Activity context;
        // so we can get the context in here for the CritAdapter later
        public void setContext(Activity context) {
            this.context = context;
        }

        @Override
        public void onClick(View view) {
            Crit crit;
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            switch (view.getId()) {
                case R.id.button_crit_bludgeon:
                    crit = dbHelper.getRandomHitCard(db, CardDatabaseHelper.HitType.BLUDGEONING);
                    break;
                case R.id.button_crit_pierce:
                    crit = dbHelper.getRandomHitCard(db, CardDatabaseHelper.HitType.PIERCING);
                    break;
                case R.id.button_crit_slash:
                    crit = dbHelper.getRandomHitCard(db, CardDatabaseHelper.HitType.SLASHING);
                    break;
                case R.id.button_crit_magic:
                    crit = dbHelper.getRandomHitCard(db, CardDatabaseHelper.HitType.MAGIC);
                    break;
                case R.id.button_fumble_melee:
                    crit = dbHelper.getRandomFumbleCard(db, CardDatabaseHelper.FumbleType.MELEE);
                    break;
                case R.id.button_fumble_natural:
                    crit = dbHelper.getRandomFumbleCard(db, CardDatabaseHelper.FumbleType.NATURAL);
                    break;
                case R.id.button_fumble_ranged:
                    crit = dbHelper.getRandomFumbleCard(db, CardDatabaseHelper.FumbleType.RANGED);
                    break;
                case R.id.button_fumble_magic:
                    crit = dbHelper.getRandomFumbleCard(db, CardDatabaseHelper.FumbleType.MAGIC);
                    break;
                default:
                    return;
            }

            if (crit != null) {
                mCritsDrawn.add(0, crit);
                Crit[] crits = new Crit[mCritsDrawn.size()];
                crits = mCritsDrawn.toArray(crits);
                ((ListView)findViewById(R.id.crits_drawn)).setAdapter(new CritAdapter(context, R.layout.drawn_crit, crits));
                if (mTtsInitListener.isTtsInitialized() && mTtsEnabled && mTtsAutoRead) {
                    Crit item = mCritsDrawn.get(0);
                    mTts.speak(sanitizeForTts(item.title + ". " + item.description), TextToSpeech.QUEUE_FLUSH, null);
                }
            } else {
                Toast toast = Toast.makeText(MainActivity.this, getString(R.string.err_no_card), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL,0,0);
                toast.show();
            }
        }
    }
    
    public class TtsInitListener implements TextToSpeech.OnInitListener {
        private boolean mIsTtsInitialized;

        @Override
        public void onInit(int status) {
            mIsTtsInitialized = (status == TextToSpeech.SUCCESS);
            if (mIsTtsInitialized && prefs.getBoolean("tts", true)) {
                if (mTts.isLanguageAvailable(Locale.ENGLISH) == TextToSpeech.LANG_AVAILABLE) {
                    mTts.setLanguage(Locale.ENGLISH);
                    mTtsEnabled = true;
                    if (!prefs.contains("tts")) {
                        Toast toast = Toast.makeText(MainActivity.this, getString(R.string.toast_tts), Toast.LENGTH_LONG);
                        toast.show();
                    }
                    mTtsAutoRead = prefs.getBoolean("auto_read",false);
                } else {
                    // start activity to install TTS data
                    Intent installIntent = new Intent();
                    installIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                    startActivity(installIntent);

                    Toast toast = Toast.makeText(MainActivity.this, getString(R.string.toast_tts_needed), Toast.LENGTH_LONG);
                    toast.show();
                }
            }
        }

        public boolean isTtsInitialized() { return mIsTtsInitialized; }
    }
}
