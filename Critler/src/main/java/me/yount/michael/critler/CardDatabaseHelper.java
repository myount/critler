package me.yount.michael.critler;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import java.io.InputStreamReader;
import java.util.Random;

public class CardDatabaseHelper extends SQLiteOpenHelper {
    final static int DATABASE_VERSION = 1;
    final static String DATABASE_NAME = "cards.db";
    Context mContext;

    public enum HitType {
        BLUDGEONING("B"), PIERCING("P"), SLASHING("S"), MAGIC("M");

        private String type;
        private HitType(String type) { this.type = type; }
        public String getAbbreviation() { return type; }
    }

    public enum FumbleType {
        MELEE("ME"), RANGED("RA"), NATURAL("NA"), MAGIC("MA");

        private String type;
        private FumbleType(String type) { this.type = type; }
        public String getAbbreviation() { return type; }
    }

    public CardDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.mContext = context;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onCreate(SQLiteDatabase db) {
        ProgressDialog progressDialog = new ProgressDialog(mContext);

        progressDialog.setTitle("Database creation in progress");
        progressDialog.setMessage("Adding initial cards to database...");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);

        runSqlFromFile(db, "create_db.sql");

        // Since onCreate is only called when the database does not exist, I'm not doing any checks
        // to see if the tables are empty or not.  If it IS called when the database exists we
        // probably have bigger problems than duplicating the cards in the database.

        try {
            AssetManager assetManager = mContext.getAssets();
            progressDialog.show();
            db.beginTransaction();

            for (String type : new String[] {"B", "P", "S", "M"}) {
                try {
                    BufferedReader in = new BufferedReader(new InputStreamReader(assetManager.open("hits" + type + ".txt", AssetManager.ACCESS_BUFFER)));
                    String line;

                    while ((line = in.readLine()) != null) {
                        String[] bits = line.split("\\|");
                        addHitCardToDb(db, bits[1], bits[2], type, false);
                    }

                    in.close();
                } catch (IOException e) {
                    Log.e("Critler", "Error importing default hit cards of type " + type, e);
                }
            }

            for (String type : new String[] {"ME", "RA", "NA", "MA"}) {
                try {
                    BufferedReader in = new BufferedReader(new InputStreamReader(assetManager.open("fumbles" + type + ".txt", AssetManager.ACCESS_BUFFER)));
                    String line;

                    while ((line = in.readLine()) != null) {
                        String [] bits = line.split("\\|");
                        addFumbleCardToDb(db, bits[1], bits[2], type, false);
                    }

                    in.close();
                } catch (IOException e) {
                    Log.e("Critler", "Error importing default fumble cards of type " + type, e);
                }
            }

            db.setTransactionSuccessful();
        } catch (SQLiteException e) {
            Log.e("Critler", "Got an SQLiteException", e);
        } finally {
            db.endTransaction();
            progressDialog.dismiss();
        }
    }

    private void addHitCardToDb(SQLiteDatabase db, String title, String description,
                                String damageType, boolean user_added) {
        Log.d("Critler", "inserting hit card into DB: \"" + title + "\", " + description
                         + "\", " + damageType + ", user_added=" + user_added);
        ContentValues cardValues = new ContentValues();
        cardValues.put("title", title);
        cardValues.put("description", description);
        cardValues.put("damage_type", damageType.getBytes());
        cardValues.put("user_added", user_added);
        cardValues.put("enabled", true);

        try {
            db.insert("hit_cards", null, cardValues);
        } catch (SQLiteException e) {
            Log.d("Critler", "Failed to insert hit card", e);
        }
    }

    private void addFumbleCardToDb(SQLiteDatabase db, String title, String description,
                                   String attackType, boolean user_added) {
        Log.d("Critler", "inserting fumble card into DB: \"" + title + "\", " + description
                + "\", " + attackType + ", user_added=" + user_added);
        ContentValues cardValues = new ContentValues();
        cardValues.put("title", title);
        cardValues.put("description", description);
        cardValues.put("attack_type", attackType.getBytes());
        cardValues.put("user_added", user_added);
        cardValues.put("enabled", true);

        try {
            db.insert("fumble_cards", null, cardValues);
        } catch (SQLiteException e) {
            Log.d("Critler", "Failed to insert fumble card", e);
        }
    }

    public Crit getHitCard(SQLiteDatabase db, int id) {
        Cursor card = db.rawQuery("SELECT * FROM hit_cards WHERE id = ?", new String[]{String.valueOf(id)});
        int colTitle = card.getColumnIndex("title"),
                colDescription = card.getColumnIndex("description"),
                colDamageType = card.getColumnIndex("damage_type"),
                icon;

        card.moveToFirst();
        icon = getDamageIcon((card.getString(colDamageType).getBytes())[0]);

        return new Crit(icon, card.getString(colTitle), card.getString(colDescription), Crit.Type.HIT);
    }

    private int getDamageIcon(byte damageType) {
        int icon = R.drawable.hit_btn;
        switch (damageType) {
            case 'B':
                icon = R.drawable.crit_bludgeon;
                break;
            case 'P':
                icon = R.drawable.crit_pierce;
                break;
            case 'S':
                icon = R.drawable.crit_slash;
                break;
            case 'M':
                icon = R.drawable.magic;
                break;
        }
        return icon;
    }

    public Crit getFumbleCard(SQLiteDatabase db, int id) {
        Cursor card = db.rawQuery("SELECT * FROM fumble_cards WHERE id = ?", new String[]{String.valueOf(id)});
        int colTitle = card.getColumnIndex("title"),
                colDescription = card.getColumnIndex("description"),
                colAttackType = card.getColumnIndex("attack_type"),
                icon;

        card.moveToFirst();
        icon = getAttackTypeIcon(card.getString(colAttackType).getBytes());

        return new Crit(icon, card.getString(colTitle), card.getString(colDescription), Crit.Type.FUMBLE);
    }

    private int getAttackTypeIcon(byte[] attackType) {
        int icon = R.drawable.hit_btn;
        switch (attackType[0]) {
            case 'M':
                switch (attackType[1]) {
                    case 'A':
                        icon = R.drawable.magic;
                        break;
                    case 'E':
                        icon = R.drawable.fumble_melee;
                        break;
                }
                break;
            case 'R':
                icon = R.drawable.fumble_ranged;
                break;
            case 'N':
                icon = R.drawable.fumble_natural;
                break;
        }
        return icon;
    }

    public Crit getRandomHitCard(SQLiteDatabase db, HitType type) {
        Cursor res = db.rawQuery("SELECT MIN(id) AS first, COUNT(id) as num FROM hit_cards " +
                                 "WHERE damage_type LIKE ?", new String[]{type.getAbbreviation()});
        int colFirst = res.getColumnIndex("first"), colNum = res.getColumnIndex("num");
        res.moveToFirst();

        if ((colFirst == -1) || (colNum == -1) || (res.getInt(colNum) == 0)) return null;

        int first = res.getInt(colFirst), num = res.getInt(colNum);
        res.close();

        Random random = new Random();
        int randid = first + random.nextInt(num);
        return getHitCard(db, randid);
    }

    public Crit getRandomFumbleCard(SQLiteDatabase db, FumbleType type) {
        Cursor res = db.rawQuery("SELECT MIN(id) AS first, COUNT(id) as num FROM fumble_cards " +
                "WHERE attack_type LIKE ?", new String[]{type.getAbbreviation()});
        int colFirst = res.getColumnIndex("first"), colNum = res.getColumnIndex("num");
        res.moveToFirst();

        if ((colFirst == -1) || (colNum == -1) || (res.getInt(colNum) == 0)) return null;

        int first = res.getInt(colFirst), num = res.getInt(colNum);
        res.close();

        Random random = new Random();
        int randid = first + random.nextInt(num);
        return getFumbleCard(db, randid);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO: implement when database schema actually changes
    }

    private void runSqlFromFile(SQLiteDatabase database, String scriptFile) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        byte[] buf = new byte[1024];
        int len;
        AssetManager assets = mContext.getAssets();
        InputStream inputStream;

        try {
            inputStream = assets.open(scriptFile);
            while ((len = inputStream.read(buf)) != -1) {
                outputStream.write(buf, 0, len);
            }
            outputStream.close();
            inputStream.close();

            String[] script = outputStream.toString().split(";");
            for (String sqlStatement : script) {
                if (sqlStatement.length() > 0) {
                    database.execSQL(sqlStatement);
                }
            }
        } catch (IOException e) {
            // TODO: handle IOException
        }
    }
}
