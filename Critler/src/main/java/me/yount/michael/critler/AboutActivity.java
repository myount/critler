package me.yount.michael.critler;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

public class AboutActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        setTitle(getString(R.string.about, getString(R.string.app_name)));

        TextView tvAboutCritler = (TextView)findViewById(R.id.about_critler);
        TextView tvAboutOgl = (TextView)findViewById(R.id.about_ogl);

        tvAboutCritler.setText(Html.fromHtml(getString(R.string.about_critler, getString(R.string.app_name), getVersion())));
        tvAboutCritler.setMovementMethod(LinkMovementMethod.getInstance());
        tvAboutOgl.setText(Html.fromHtml(getString(R.string.about_ogl)));
    }

    private String getVersion() {
        try {
            return getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            // if this ever gets thrown there are probably bigger problems than my not catching it
            // properly
            e.printStackTrace();
            return "vX.X.X";
        }
    }
}