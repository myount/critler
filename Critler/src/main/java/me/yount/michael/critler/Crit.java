package me.yount.michael.critler;

public class Crit {
    public enum Type {
        HIT(0xffff0000), FUMBLE(0xffffff00);

        private int color;
        private Type(int color) { this.color = color; }
        public int getColor() { return this.color; }
    }
    public int icon = 0;
    private Type type;
    public String title;
    public String description;

    public Crit(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public Crit(int icon, String title, String description) {
        this(title, description);
        this.icon = icon;
    }

    public Crit(int icon, String title, String description, Type type) {
        this(icon, title, description);
        this.type = type;
    }

    public Type getType() { return type; }
}
