CREATE TABLE hit_cards (
     id          INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
     damage_type CHAR(1) NOT NULL,
     title       TEXT    NOT NULL,
     description TEXT    NOT NULL,
     user_added  BOOLEAN NOT NULL,
     enabled     BOOLEAN NOT NULL
);

CREATE TABLE fumble_cards (
     id          INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
     attack_type CHAR(2) NOT NULL,
     title       TEXT    NOT NULL,
     description TEXT    NOT NULL,
     user_added  BOOLEAN NOT NULL,
     enabled     BOOLEAN NOT NULL
);