P|Elbow Pierced|Double damage and target drops whatever is being held (1 item).
P|Lodged in the Bone|Double damage and 2 bleed. This bleed requires a DC 20 Heal check to stop.
P|Punctured Lung|Double damage and target begins to drown 3 rounds later. DC 20 Fort save each round to end effect.
P|Nerve Cluster|Normal damage and target is stunned for 1d6 rounds (Fort save each round to act).
P|Muscle Severed|Normal damage and 1d6 Dex and 1d6 Str damage.
P|Forearm Piercing|Double damage and target is disarmed (1 item).
P|Cheek Pierced|Normal damage and 1d4 bleed. Target gains 50% spell failure chance for verbal spells until healed.
P|Spinal Tap|Normal damage and -4 penalty on attack rolls, skill checks, and ability checks for 1d4 rounds.
P|Painful Poke|Double damage and target can take only one move or standard action next round.
P|Knockback|Double damage and target is pushed 1d6 squares directly away.
P|Bleeder|Double damage and 1d6 bleed.
P|Deep Hurting|Double damage and target is fatigued.
P|Infection|Double damage and target contracts Filth fever (Fort negates).
P|Pierced|Double damage and target is dazed for 1 round.
P|Ventilated|Double damage and 2d6 nonlethal damage.
P|Pinned Arm|Double damage and one arm cannot move (DC 20 Str or Heal to be freed).
P|Sucking Chest Wound|Double damage and target is exhausted (Fort negates).
P|Stinger|Normal damage and target is sickened for 1d6 rounds.
P|Vulnerable Spot|Normal damage and target takes 1d4 ability damage of your choice.
P|In a Row|Double damage to target and normal damage to adjacent target.
P|Heart Shot|Triple damage and 1 Con bleed.
P|Nailed in Place|Double damage and target cannot move (DC 20 Str check negates).
P|Achilles Heel|Normal damage and 1d2 Dex damage. Target's speeds are reduced by half until healed.
P|Penetrating Wound|Double damage and ignore DR.
P|Grazing Hit|Normal damage and target is stunned for 1 round.
P|Eye Patch for You|Triple damage and 1d2 Con drain, -4 on Spot, Search and ranged attacks until healed.
P|Spun Around|Normal damage and target is flat-footed for 1 round.
P|Left Reeling|Double damage and target takes a -2 penalty to AC for 1d4 rounds.
P|Bicep Wound|Normal damage and 1d4 Str damage.
P|Guarded Strike|Double damage and +4 to your AC for 1 round.
P|Ragged Wound|Normal damage and 1d8 bleed.
P|Perfect Strike|Triple damage.
P|Tenacious Wound|Normal damage and 1d2 Con damage. Target does not heal this damage naturally.
P|Leg Wound|Double damage and target's land speed is halved for 1d4 rounds.
P|Organ Scramble|Double damage and 1d6 Con damage.
P|Deep Wound|Double damage and target is nauseated for 1 round (Fort negates).
P|Pinhole|Double damage and 1 bleed. Target takes 1 additional bleed each round until healed.
P|Right in the Ear|Normal damage and 1 Int damage and 1 bleed.
P|Appendicitis|Double damage and target is poisoned (treat as greenblood oil).
P|Blown Back|Double damage and target is knocked prone.
P|Momentum|Double damage and +2 on all your attack rolls for 1 round.
P|Shoulder Wound|Double damage and 1d2 Str and Dex damage.
P|Tongue Piercing|Normal damage and 1 Con damage. Target gains 50% spell failure chance for verbal spells until healed.
P|Surprise Opening|Double damage and one free attack against the target with a -5 penalty.
P|Calf Hole|Normal damage and 1d4 Dex damage.
P|Kidney Piercing|Double damage and target sickened for 2d4 rounds (Fort negates).
P|Chipped Bone|Double damage and 1 Dex damage.
P|Nicked an Artery|Normal damage and 2d6 bleed.
P|Javelin Catcher|Double damage and 1d6 bleed if from ranged attack.
P|Clean Through|Normal damage and 1d6 bleed and 1 Con drain.
P|Overreaction|Normal damage and target provokes attacks of opportunity from all threatening opponents.
P|Hand Wound|Normal damage and 1d2 Dex damage. -4 penalty on all rolls using that hand until healed.
